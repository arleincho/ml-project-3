(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 56)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 56
  });

  function closeSnoAlertBox(){
    window.setTimeout(function () {
        $("#snoAlertBox").fadeOut(300)
    }, 3000);
}

    var models = [];

    $('#translate').on('click', function(event) {
        models = [];
        event.preventDefault(); // To prevent following the link (optional)
        $("input[type='checkbox']:checked").each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal != ""){
                models.push(sThisVal)
            }
        });

        $('#result').html('');
        if (models.length > 0 && $("#input").val() != ""){
            $('#translate').html('Translating......');
            $('#input').attr('disabled', '');
            $.ajax({
                type: "POST",
                url: "http://localhost:8000/",
                data: JSON.stringify({'models': models, 'input': $("#input").val()}),
                dataType: 'json',
                success: function(data){
                    $('#result').html(data);
                    $('#translate').html('Translate');
                    $('#input').removeAttr('disabled');
                    
                    if ('result' in data){

                        var html = '<br><ul>';
                        $.each(data['result'], function(key, value){
                            var div = $("#" + key).siblings("div");
                            if (div){
                                 html += '<dt>' + $("label", div).html() + '</dt><dd> - ' + value + '</dd>';
                            }
                        })
                        html += '</ul>';
                        $('#result').append(html);
                    }
                }
            });
        }else{
            $("#snoAlertBox").fadeIn();
            closeSnoAlertBox();
        }
    });

})(jQuery); // End of use strict
