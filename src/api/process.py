#!/usr/bin/env python3


import os
import pickle
import numpy as np


from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import load_model


simple_rnn_model = load_model('/pickles/model1.h5')
embed_rnn_model = load_model('/pickles/model2.h5')
bd_rnn_model = load_model('/pickles/model3.h5')
bd_emb_rnn_model = load_model('/pickles/model4.h5')
embed_lstm_model = load_model('/pickles/model5.h5')


with open('/pickles/english_tokenizer.pickle2', 'rb') as pickle_file:
    english_tokenizer = pickle.load(pickle_file)


with open('/pickles/french_tokenizer.pickle2', 'rb') as pickle_file:
    french_tokenizer = pickle.load(pickle_file)



def get_model(argument):

    models = {
        "simple_rnn_model": simple_rnn_model,
        "embed_rnn_model": embed_rnn_model,
        "bd_rnn_model": bd_rnn_model,
        "bd_emb_rnn_model": bd_emb_rnn_model,
        "embed_lstm_model": embed_lstm_model
    }

    return models.get(argument, None)



def pad(x, length=None):
    """
    Pad x
    :param x: List of sequences.
    :param length: Length to pad the sequence to.  If None, use length of longest sequence in x.
    :return: Padded numpy array of sequences
    """
    # TODO: Implement
    return pad_sequences(x, maxlen=length, padding='post')


def tokenize(x):
    """
    Tokenize x
    :param x: List of sentences/strings to be tokenized
    :return: Tuple of (tokenized x data, tokenizer used to tokenize x)
    """
    # TODO: Implement
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(x)
    return tokenizer.texts_to_sequences(x), tokenizer


# used to convert predicted token (by model) to word text
# get the index of the node that has the high probability (softmax) to fetch the token text
def logits_to_text(logits, tokenizer):
    """
    Turn logits from a neural network into text using the tokenizer
    :param logits: Logits from a neural network
    :param tokenizer: Keras Tokenizer fit on the labels
    :return: String that represents the text of the logits
    """
    index_to_words = {id: word for word, id in tokenizer.word_index.items()}
    index_to_words[0] = '<PAD>'

    return ' '.join([index_to_words[prediction] for prediction in np.argmax(logits, 1)])




def text_to_token_Preprocessing(text, english_tokenizer, modelNumber):
    strList = [text]
    text_tokenized, text_tokenizer = tokenize(strList)
    # we have only to get the keys 
    wordList = list(text_tokenizer.word_index.keys()) 
    tokenList =[]
    for word in wordList:
        tokenList.append(english_tokenizer.word_index.get(word,0)) 
        #if word doesn't exist in vocabulary, we treat it as <pad>
    
    # if input text length doesn't fit our input shape
    max_french_sequence_length = 21
    if len(tokenList) > max_french_sequence_length:
        tokenList = tokenList[:max_french_sequence_length]
            
    tokens = [tokenList]
    if modelNumber == "simple_rnn_model" or modelNumber == "bd_rnn_model":
        tokens = pad(tokens, max_french_sequence_length) 
        tokens = tokens.reshape((-1, max_french_sequence_length, 1))
    else:
        tokens = pad(tokens, max_french_sequence_length) 
        tokens = tokens.reshape((-1, max_french_sequence_length))
    

    return tokens



def translate(text, model):
    # testing_input = text_to_token_Preprocessing("he was driving a good truck ", english_tokenizer, 1)

    model_translate = get_model(model)
    translated = None
    if model_translate is not None:
        testing_input = text_to_token_Preprocessing(text, english_tokenizer, model)
        translated = logits_to_text(model_translate.predict(testing_input)[0], french_tokenizer)

    return translated.replace("<PAD>", "").strip()

