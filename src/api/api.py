#!/usr/bin/env python3
#
# Copyright 2009 Facebook
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.


import os.path
import re
import tornado.escape
import tornado.httpserver
import tornado.ioloop
import tornado.locks
import tornado.web
import json

from process import translate




api_port = os.environ['API_PORT']


class Application(tornado.web.Application):
    def __init__(self):

        handlers = [
            (r"/", ContentBaseHandler)
        ]

        settings = dict(
            cookie_secret="Movb9mdCSSCjI3SoOjNOrcRoXhfZdz04RqFJiUdDWsp3EOPvjVTOYkHyR5exwP5Z22rpM",
            debug=True,
        )
        super(Application, self).__init__(handlers, **settings)


class BaseHandler(tornado.web.RequestHandler):

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Credentials", "true")
        self.set_header("Access-Control-Allow-Headers", "cache-control,Content-Type,X-Amz-Date,Authorization,X-Api-Key,Origin,Accept,Access-Control-Allow-Headers,Access-Control-Allow-Methods,Access-Control-Allow-Origin")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')


  
class ContentBaseHandler(BaseHandler):

    async def post(self):
        try:
            # params = { k: self.get_argument(k) for k in self.request.arguments }

            params = tornado.escape.json_decode(self.request.body)

            result = {}
            if 'models' in params and type(params['models']) == list:
                for model in params['models']:
                    result[model] = translate(params['input'], model)

            self.write({"result": result})

        except Exception:
            raise tornado.web.HTTPError(500)
            return
  

async def main():

    # Create the global connection pool.
    app = Application()
    app.listen(api_port)

        # In this demo the server will simply run until interrupted
        # with Ctrl-C, but if you want to shut down more gracefully,
        # call shutdown_event.set().
    shutdown_event = tornado.locks.Event()
    await shutdown_event.wait()


if __name__ == "__main__":
    tornado.ioloop.IOLoop.current().run_sync(main)
